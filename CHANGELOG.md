# Next

- **[Breaking change]** Drop `lib` prefix and `.js` extension from deep-imports.
- **[Internal]** Refactor project structure.

# 0.1.4 (2019-12-28)

- **[Fix]** Update dependencies.
- **[Fix]** Update hidden items.

# 0.1.3 (2018-12-31)

- **[Fix]** Do not emit hidden items by default.

# 0.1.2 (2018-12-26)

- **[Feature]** Add Ounenohn's family.
- **[Internal]** Update CI to use Node 11.3 and better support shallow repositories.

# 0.1.1 (2018-12-03)

- **[Fix]** Update dependencies.
- **[Internal]** Update continuous deployment script.

# 0.1.0 (2018-01-30)

- **[Feature]** First release.
