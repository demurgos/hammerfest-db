import chai from "chai";

import { getFamilies, getFamilyById, getItemById, getItems } from "../lib/items.js";
import { Family } from "../lib/types/family.js";
import { Item } from "../lib/types/item.js";
import { ItemType } from "../lib/types/item-type.js";
import { ScoreItem } from "../lib/types/score-item.js";

describe("Items", function () {
  it("should return 353 items for `getItems` (without hidden)", function () {
    const actual: Item[] = [...getItems()];
    chai.assert.lengthOf(actual, 353);
  });

  it("should return 357 items for `getItems` (with hidden)", function () {
    const actual: Item[] = [...getItems(true)];
    chai.assert.lengthOf(actual, 357);
  });

  it("Item 1000", function () {
    const actual: Item = getItemById("1000");
    const expected: Item = {
      id: "1000",
      familyId: "1000",
      type: ItemType.Score,
      unlock: 10,
      rarity: 0,
      value: {
        max: 50000,
        pow: 2,
        base: 500,
      },
    };
    chai.assert.deepEqual(actual, expected);
  });
});

describe("Families", function () {
  it("should return 51 families for `getFamilies` (without hidden)", function () {
    const actual: Family[] = [...getFamilies()];
    chai.assert.lengthOf(actual, 51);
  });

  it("should return 52 families for `getFamilies` (with hidden)", function () {
    const actual: Family[] = [...getFamilies(true)];
    chai.assert.lengthOf(actual, 52);
  });

  it("Family 1003", function () {
    const actual: Family = getFamilyById("1003");
    const expected: Family = {
      id: "1003",
      hidden: false,
      items: new Set([
        <ScoreItem> {
          id: "1009",
          familyId: "1003",
          type: ItemType.Score,
          unlock: 10,
          rarity: 1,
          value: 5000,
        },
        <ScoreItem> {
          id: "1010",
          familyId: "1003",
          type: ItemType.Score,
          unlock: 10,
          rarity: 2,
          value: 15000,
        },
        <ScoreItem> {
          id: "1011",
          familyId: "1003",
          type: ItemType.Score,
          unlock: 10,
          rarity: 3,
          value: 25000,
        },
      ]),
    };
    chai.assert.deepEqual(actual, expected);
  });
});
