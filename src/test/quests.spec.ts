import chai from "chai";

import { getQuestById, getQuests } from "../lib/quests.js";
import { Quest } from "../lib/types/quest.js";
import { QuestRewardType } from "../lib/types/quest-reward-type.js";
import { FamilyQuestReward } from "../lib/types/quest-rewards.js";

describe("Quests", function () {
  it("should return 76 quests for `getQuests`", function () {
    const actual: Quest[] = [...getQuests()];
    chai.assert.lengthOf(actual, 76);
  });

  it("Quest 0", function () {
    const actual: Quest = getQuestById("0");
    const expected: Quest = {
      id: "0",
      require: new Map([
        ["40", 1],
        ["41", 1],
        ["42", 1],
        ["43", 1],
        ["44", 1],
        ["45", 1],
        ["46", 1],
        ["47", 1],
        ["48", 1],
        ["49", 1],
        ["50", 1],
        ["51", 1],
      ]),
      give: new Set([
        <FamilyQuestReward> {type: QuestRewardType.Family, id: "100"},
        <FamilyQuestReward> {type: QuestRewardType.Family, id: "6"},
      ]),
      remove: new Set([
        <FamilyQuestReward> {type: QuestRewardType.Family, id: "5"},
      ]),
    };
    chai.assert.deepEqual(actual, expected);
  });
});
