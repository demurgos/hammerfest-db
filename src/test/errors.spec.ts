import chai from "chai";

import { getFamilyById, getItemById } from "../lib/items.js";
import { getQuestById } from "../lib/quests.js";

function assertThrows(fn: () => void): Error | never {
  try {
    fn();
  } catch (err) {
    return err;
  }
  return chai.assert.fail(undefined, undefined, "expected [Function] to throw an error") as never;
}

describe("errors", function () {
  it("Should throw an ItemNotFound error for invalid itemId", function () {
    const error: Error = assertThrows(() => getItemById("1000000"));
    chai.assert.deepPropertyVal(error, "name", "ItemNotFound");
    chai.assert.deepPropertyVal(error, "data", {id: "1000000"});
    // TODO: Remove this test once instanbul allows to ignore function declarations in ESM
    chai.assert.deepPropertyVal(error, "message", "Item not found for the ID: 1000000");
  });
  it("Should throw a FamilyNotFound error for invalid familyId", function () {
    const error: Error = assertThrows(() => getFamilyById("-1"));
    chai.assert.deepPropertyVal(error, "name", "FamilyNotFound");
    chai.assert.deepPropertyVal(error, "data", {id: "-1"});
    // TODO: Remove this test once instanbul allows to ignore function declarations in ESM
    chai.assert.deepPropertyVal(error, "message", "Family not found for the ID: -1");
  });
  it("Should throw a QuestNotFound error for invalid questId", function () {
    const error: Error = assertThrows(() => getQuestById("aaa"));
    chai.assert.deepPropertyVal(error, "name", "QuestNotFound");
    chai.assert.deepPropertyVal(error, "data", {id: "aaa"});
    // TODO: Remove this test once instanbul allows to ignore function declarations in ESM
    chai.assert.deepPropertyVal(error, "message", "Quest not found for the ID: aaa");
  });
});
