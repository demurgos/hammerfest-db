import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum QuestRewardType {
  Bank,
  Family,
  Log,
  Mode,
  Option,
  Tokens,
}

export const $QuestRewardType: TsEnumType<QuestRewardType> = new TsEnumType<QuestRewardType>({
  enum: QuestRewardType,
  changeCase: CaseStyle.KebabCase,
});
