import { FamilyId } from "./family-id.js";
import { ScoreItem } from "./score-item.js";
import { SpecialItem } from "./special-item.js";

export interface Family {
  id: FamilyId;
  /**
   * Boolean indicating that the family is "hidden`.
   * It corresponds to the items 1186, 1187, 1188 and 1189.
   */
  hidden: boolean;
  items: Set<ScoreItem | SpecialItem>;
}
