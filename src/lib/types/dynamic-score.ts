/**
 * Represents the parameters for computed values: Math.min(max, base * Math.pow(i + 1, pow))
 */
export interface DynamicScore {
  max: number;
  pow: number;
  base: number;
}
