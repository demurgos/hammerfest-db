import { IntegerType } from "kryo/integer";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";

import { FamilyId } from "./family-id.js";
import { $QuestRewardType, QuestRewardType } from "./quest-reward-type.js";

/**
 * Increses the amount of credits received at the bank.
 */
export interface BankQuestReward {
  type: QuestRewardType.Bank;
  /**
   * Percentage.
   * Example: `25` means `25` percent.
   */
  value: number;
}

export const $BankQuestReward: RecordIoType<BankQuestReward> = new RecordType<BankQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Bank>({type: $QuestRewardType, value: QuestRewardType.Bank})},
    value: {type: new IntegerType({min: 0, max: 100})},
  },
});

export interface FamilyQuestReward {
  type: QuestRewardType.Family;
  id: FamilyId;
}

export const $FamilyQuestReward: RecordIoType<FamilyQuestReward> = new RecordType<FamilyQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Family>({type: $QuestRewardType, value: QuestRewardType.Family})},
    id: {type: new Ucs2StringType({maxLength: 10})},
  },
});

export interface LogQuestReward {
  type: QuestRewardType.Log;
  id: string;
}

export const $LogQuestReward: RecordIoType<LogQuestReward> = new RecordType<LogQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Log>({type: $QuestRewardType, value: QuestRewardType.Log})},
    id: {type: new Ucs2StringType({maxLength: 10})},
  },
});

export interface ModeQuestReward {
  type: QuestRewardType.Mode;
  key: string;
}

export const $ModeQuestReward: RecordIoType<ModeQuestReward> = new RecordType<ModeQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Mode>({type: $QuestRewardType, value: QuestRewardType.Mode})},
    key: {type: new Ucs2StringType({maxLength: 20})},
  },
});

export interface OptionQuestReward {
  type: QuestRewardType.Option;
  key: string;
}

export const $OptionQuestReward: RecordIoType<OptionQuestReward> = new RecordType<OptionQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Option>({type: $QuestRewardType, value: QuestRewardType.Option})},
    key: {type: new Ucs2StringType({maxLength: 20})},
  },
});

export interface TokensQuestReward {
  type: QuestRewardType.Tokens;
  value: number;
}

export const $TokensQuestReward: RecordIoType<TokensQuestReward> = new RecordType<TokensQuestReward>({
  properties: {
    type: {type: new LiteralType<QuestRewardType.Tokens>({type: $QuestRewardType, value: QuestRewardType.Tokens})},
    value: {type: new IntegerType({min: 0, max: 100})},
  },
});
