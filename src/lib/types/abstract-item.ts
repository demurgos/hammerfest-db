import { FamilyId } from "./family-id.js";
import { ItemId } from "./item-id.js";
import { ItemType } from "./item-type.js";

export interface AbstractItem {
  type: ItemType;

  id: ItemId;

  /**
   * Number of items to collect to unlock it in the inventory.
   */
  unlock: number;

  /**
   * Rarity coefficient, between 0 and 7 (inclusive).
   */
  rarity: number;

  familyId: FamilyId;
}
