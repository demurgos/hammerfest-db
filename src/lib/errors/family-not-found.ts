import { Incident } from "incident";

import { FamilyId } from "../types/family-id.js";

export type Name = "FamilyNotFound";
export const name: Name = "FamilyNotFound";

export interface Data {
  id: FamilyId;
}

export type Cause = undefined;
export type FamilyNotFoundError = Incident<Data, Name, Cause>;

export function format({id}: Data): string {
  return `Family not found for the ID: ${id}`;
}

export function createFamilyNotFoundError(id: FamilyId): FamilyNotFoundError {
  return Incident(name, {id}, format);
}
