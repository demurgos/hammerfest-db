import { Incident } from "incident";

import { ItemId } from "../types/item-id.js";

export type Name = "ItemNotFound";
export const name: Name = "ItemNotFound";

export interface Data {
  id: ItemId;
}

export type Cause = undefined;
export type ItemNotFoundError = Incident<Data, Name, Cause>;

export function format({id}: Data): string {
  return `Item not found for the ID: ${id}`;
}

export function createItemNotFoundError(id: ItemId): ItemNotFoundError {
  return Incident(name, {id}, format);
}
