import { readFileSync } from "fs";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";

import { getQuestsDbPath } from "./db/db.js";
import { createQuestNotFoundError } from "./errors/quest-not-found.js";
import { ItemId } from "./types/item-id.js";
import { Quest } from "./types/quest.js";
import { QuestId } from "./types/quest-id.js";
import { $QuestReward, QuestReward } from "./types/quest-reward.js";
import { $FamilyQuestReward, FamilyQuestReward } from "./types/quest-rewards.js";

function* readQuests(dbQuests: Iterable<any>): Iterable<Quest> {
  for (const dbQuest of dbQuests) {
    const require: Map<ItemId, number> = new Map();
    for (const itemId in dbQuest.require) {
      require.set(itemId, dbQuest.require[itemId]);
    }
    const give: Set<QuestReward> = new Set();
    for (const dbReward of dbQuest.give) {
      give.add($QuestReward.read(JSON_VALUE_READER, dbReward));
    }
    const remove: Set<FamilyQuestReward> = new Set();
    for (const dbReward of dbQuest.remove) {
      const reward: FamilyQuestReward = $FamilyQuestReward.read(JSON_VALUE_READER, dbReward);
      remove.add(reward);
    }

    yield {
      id: dbQuest.id,
      require,
      give,
      remove,
    };
  }
}

const quests: Map<QuestId, Quest> = new Map();
const questsDbPath: string = getQuestsDbPath();
const questsDbString: string = readFileSync(questsDbPath).toString("utf8");
const dbItems: any[] = JSON.parse(questsDbString);
for (const quest of readQuests(dbItems)) {
  quests.set(quest.id, quest);
}

export function getQuests(): Iterable<Quest> {
  return quests.values();
}

export function getQuestById(questId: QuestId): Quest {
  const result: Quest | undefined = quests.get(questId);
  if (result === undefined) {
    throw createQuestNotFoundError(questId);
  }
  return result;
}
