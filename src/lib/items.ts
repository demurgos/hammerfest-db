import fs from "fs";

import { getFamiliesDbPath } from "./db/db.js";
import { createFamilyNotFoundError } from "./errors/family-not-found.js";
import { createItemNotFoundError } from "./errors/item-not-found.js";
import { Family } from "./types/family.js";
import { FamilyId } from "./types/family-id.js";
import { Item } from "./types/item.js";
import { ItemId } from "./types/item-id.js";
import { ItemType } from "./types/item-type.js";

function readItem(dbItem: any, familyId: FamilyId): Item {
  if (parseInt(dbItem.id, 10) < 1000) {
    return {
      id: dbItem.id,
      type: ItemType.Special,
      familyId,
      rarity: dbItem.rarity,
      unlock: dbItem.unlock !== undefined ? dbItem.unlock : 10,
    };
  } else {
    return {
      id: dbItem.id,
      type: ItemType.Score,
      familyId,
      rarity: dbItem.rarity,
      value: dbItem.value,
      unlock: dbItem.unlock !== undefined ? dbItem.unlock : 10,
    };
  }
}

function* readFamilies(dbFamilies: Iterable<any>): Iterable<Family> {
  for (const dbFamily of dbFamilies) {
    const familyId: FamilyId = dbFamily.id;
    const hidden: boolean = dbFamily.hidden !== undefined ? dbFamily.hidden : false;
    const items: Set<Item> = new Set(dbFamily.items.map((dbItem: any) => readItem(dbItem, familyId)));
    yield {id: familyId, hidden, items};
  }
}

const families: Map<FamilyId, Family> = new Map();
const items: Map<ItemId, Item> = new Map();
const HIDDEN_ITEMS: Set<ItemId> = new Set();
const familiesDbPath: string = getFamiliesDbPath();
const familiesDbString: string = fs.readFileSync(familiesDbPath).toString("utf8");
const dbFamilies: any[] = JSON.parse(familiesDbString);
for (const family of readFamilies(dbFamilies)) {
  families.set(family.id, family);
  for (const item of family.items) {
    items.set(item.id, item);
    if (family.hidden) {
      HIDDEN_ITEMS.add(item.id);
    }
  }
}

export function* getFamilies(withHidden: boolean = false): Iterable<Family> {
  if (withHidden) {
    yield* families.values();
  } else {
    for (const family of families.values()) {
      if (!family.hidden) {
        yield family;
      }
    }
  }
}

export function* getItems(withHidden: boolean = false): Iterable<Item> {
  if (withHidden) {
    yield* items.values();
  } else {
    for (const item of items.values()) {
      if (!HIDDEN_ITEMS.has(item.id)) {
        yield item;
      }
    }
  }
}

export function getItemById(itemId: ItemId): Item {
  const result: Item | undefined = items.get(itemId);
  if (result === undefined) {
    throw createItemNotFoundError(itemId);
  }
  return result;
}

export function getFamilyById(familyId: FamilyId): Family {
  const result: Family | undefined = families.get(familyId);
  if (result === undefined) {
    throw createFamilyNotFoundError(familyId);
  }
  return result;
}
