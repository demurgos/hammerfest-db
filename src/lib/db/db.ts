import sysPath from "path";
import { fileURLToPath } from "url";

const SELF_PATH: string = fileURLToPath(import.meta.url);
const PACKAGE_ROOT: string = sysPath.join(SELF_PATH, "../../..");

export function getFamiliesDbPath(): string {
  return sysPath.resolve(PACKAGE_ROOT, "db", "families.json");
}

export function getQuestsDbPath(): string {
  return sysPath.resolve(PACKAGE_ROOT, "db", "quests.json");
}
