"use strict";

const cheerio = require("cheerio");
const fs = require("fs");
const {Incident} = require("incident");
const sysPath = require("path");

const projectRoot = sysPath.resolve(__dirname, "..");
const rawDir = sysPath.join(projectRoot, "raw", "pretty");
const dbDir = sysPath.join(projectRoot, "src", "lib", "db");

function getXml(fileName) {
  return cheerio.load(fs.readFileSync(sysPath.join(rawDir, fileName)).toString("utf8"), {xmlMode: true});
}

const langEnXml = getXml("lang.en.xml");
const langEsXml = getXml("lang.es.xml");
const langFrXml = getXml("lang.fr.xml");
const linksXml = getXml("links.xml");
const questsXml = getXml("quests.xml");
const specialItemsXml = getXml("special-items.xml");
const scoreItemsXml = getXml("score-items.xml");

const messages = {};

const quests = [];
questsXml("quest").each((i, _questElem) => {
  const questElem = questsXml(_questElem);
  const id = questElem.attr("id");
  const _require = {};
  const give = [];
  const remove = [];

  questElem.children("require").each((i, _requireElem) => {
    const requireElem = questsXml(_requireElem);
    _require[requireElem.attr("item")] = parseInt(requireElem.attr("qty"), 10);
  });
  questElem.children("give").each((i, giveElem) => {
    give.push(getQuestReward(questsXml(giveElem)));
  });
  questElem.children("remove").each((i, removeElem) => {
    remove.push(getQuestReward(questsXml(removeElem)));
  });

  quests.push({id, require: _require, give, remove});
});

function getQuestReward(rewardElem) {
  if (rewardElem.attr("family") !== undefined) {
    return {type: "family", id: questsXml(rewardElem).attr("family")}
  } else if (rewardElem.attr("mode") !== undefined) {
    return {type: "mode", key: questsXml(rewardElem).attr("mode")}
  } else if (rewardElem.attr("option") !== undefined) {
    return {type: "option", key: questsXml(rewardElem).attr("option")}
  } else if (rewardElem.attr("log") !== undefined) {
    return {type: "log", id: questsXml(rewardElem).attr("log")}
  } else if (rewardElem.attr("tokens") !== undefined) {
    return {type: "tokens", value: parseInt(questsXml(rewardElem).attr("tokens"), 10)}
  } else if (rewardElem.attr("bank") !== undefined) {
    return {type: "bank", value: parseInt(questsXml(rewardElem).attr("bank"), 10)}
  } else {
    throw new Incident("UnreachableCode", `Unexpected quest reward: ${rewardElem}`);
  }
}

const families = [];
processItems(scoreItemsXml, true);
processItems(specialItemsXml, false);

function processItems(itemsXml, isScore) {
  itemsXml("family").each((i, _familyElem) => {
    const familyElem = questsXml(_familyElem);
    const familyId = familyElem.attr("id");
    const hiddenAttr = familyElem.attr("hidden");
    const hidden = hiddenAttr !== undefined ? hiddenAttr.toString().trim() === "hidden" : undefined;
    const items = [];

    familyElem.children("item").each((i, _itemElem) => {
      const itemElem = questsXml(_itemElem);
      const itemId = itemElem.attr("id");
      const unlockAttr = itemElem.attr("unlock");
      const unlock = unlockAttr !== undefined ? parseInt(unlockAttr, 10) : undefined;
      const rarity = parseInt(itemElem.attr("rarity"), 10);
      if (isScore) {
        let value = parseInt(itemElem.attr("value"), 10);
        if (isNaN(value)) {
          // Computed values use: Math.min(max, base * Math.pow(i + 1, pow))
          switch (itemId) {
            // Hammerfest Crystal
            case "1000":
              value = {max: 50000, pow: 2, base: 500};
              break;
            // Difaïned Diamond
            case "1008":
              value = 2000;
              break;
            // Magical Stones
            case "1017":
              value = {max: 10000, pow: 4, base: 75};
              break;
            default:
              throw new Incident("UnreachableCode");
          }
        }
        items.push({id: itemId, rarity, value, unlock});
      } else {
        items.push({id: itemId, rarity, unlock});
      }
    });
    families.push({id: familyId, type: isScore ? "score" : "special", hidden, items});
  });
}

function writeJson(fileName, data) {
  return fs.writeFileSync(sysPath.join(dbDir, fileName), JSON.stringify(data, null, 2));
}

writeJson("families.json", families);
writeJson("quests.json", quests);
